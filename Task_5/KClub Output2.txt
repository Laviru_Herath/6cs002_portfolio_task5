   clubName                pointTotal    wonTotal  lostTotal   drawnTotal  
   ---------                ------    ---------  ----------   ------
1  Lumberjacks                621       400        76         1
2  Hull and Oates             625       414        75         0
3  Bearcats                   453       421        68         1
4  Sharknadoes                664       418        68         1
5  Timberwolves               663       437        68         0
6  Pine Riders                672       527        61         2
7  Freezer Geezers            497       482        54         0
8  Money Shots                444       514        49         0
9  Warlocks                   553       575        48         1
10 Spartans                   442       578        40         1
11 Renegades                  475       545        34         1
12 Mid Ice Crisis             223      1021         1         0

 Parallel Stream 
   clubName                pointTotal    wonTotal  lostTotal   drawnTotal  
   ---------                ------    ---------  ----------   ------
8  Money Shots                444       514        49         0
2  Hull and Oates             625       414        75         0
9  Warlocks                   553       575        48         1
3  Bearcats                   453       421        68         1
11 Renegades                  475       545        34         1
7  Freezer Geezers            497       482        54         0
10 Spartans                   442       578        40         1
6  Pine Riders                672       527        61         2
12 Mid Ice Crisis             223      1021         1         0
4  Sharknadoes                664       418        68         1
5  Timberwolves               663       437        68         0
1  Lumberjacks                621       400        76         1
