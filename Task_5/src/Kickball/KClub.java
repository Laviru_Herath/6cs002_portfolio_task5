package Kickball;

/******************************************************************************
 * This class stores the details of a Premiership rugby club including the 
 * performance measures that determine their position in the league. The class 
 * implements the Comparable interface, which determines how clubs will be 
 * sorted.     
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class KClub implements Comparable<KClub> {
  private int gamePlace;
  private String clubName;
  private int playedMatches;
  private int wonTotal;
  private int drawnTotal;
  private int lostTotal;
  private int pointsScored;
  private int pointsMissed;
  private int pointsDifference;
  private int triesScores;
  private int triesMissed;
  private int attemptBonus;
  private int losingBonus;
  private int pointTotal;

  public KClub(int gamePlace, String clubName, int playedMatches, int wonTotal, int drawnTotal,
      int lostTotal, int pointsScored, int pointsMissed, int pointsDifference,
      int triesScores, int triesMissed, int attemptBonus, int losingBonus,
      int pointTotal) {
    this.gamePlace = gamePlace;
    this.clubName = clubName;
    this.playedMatches = playedMatches;
    this.wonTotal = wonTotal;
    this.drawnTotal = drawnTotal;
    this.lostTotal = lostTotal;
    this.pointsScored = pointsScored;
    this.pointsMissed = pointsMissed;
    this.pointsDifference = pointsDifference;
    this.triesScores = triesScores;
    this.triesMissed = triesMissed;
    this.attemptBonus = attemptBonus;
    this.losingBonus = losingBonus;
    this.pointTotal = pointTotal;
  }

  public String toString() {
    return String.format("%-3d%-20s%10d%10d%10d%10d", gamePlace, clubName, pointsScored,
        pointsMissed, pointTotal, drawnTotal);
  }

  public int getgamePlace() {
    return gamePlace;
  }

  public void setgamePlace(int gamePlace) {
    this.gamePlace = gamePlace;
  }

  public String getClubName() {
    return clubName;
  }

  public void setClub(String clubName) {
    this.clubName = clubName;
  }

  public int getPlayedMatches() {
    return playedMatches;
  }

  public void setPlayedMatches(int playedMatches) {
    this.playedMatches = playedMatches;
  }

  public int getWonTotal() {
    return wonTotal;
  }

  public void setWonTotal(int wonTotal) {
    this.wonTotal = wonTotal;
  }

  public int getDrawnTotal() {
    return drawnTotal;
  }

  public void setDrawnTotal(int drawnTotal) {
    this.drawnTotal = drawnTotal;
  }

  public int getLostTotal() {
    return lostTotal;
  }

  public void setLostTotal(int lostTotal) {
    this.lostTotal = lostTotal;
  }

  public int getPointsScored() {
    return pointsScored;
  }

  public void setPointsScored(int pointsScored) {
    this.pointsScored = pointsScored;
  }

  public int getPointsMissed() {
    return pointsMissed;
  }

  public void setPointsMissed(int pointsMissed) {
    this.pointsMissed = pointsMissed;
  }

  public int getPointsDifference() {
    return pointsDifference;
  }

  public void setPointsDifference(int pointsDifference) {
    this.pointsDifference = pointsDifference;
  }

  public int getTriesScores() {
    return triesScores;
  }

  public void setTriesFor(int triesScores) {
    this.triesScores = triesScores;
  }

  public int getTriesMissed() {
    return triesMissed;
  }

  public void setTriesAgainst(int triesMissed) {
    this.triesMissed = triesMissed;
  }

  public int getattemptBonus() {
    return attemptBonus;
  }

  public void setattemptBonus(int attemptBonus) {
    this.attemptBonus = attemptBonus;
  }

  public int getLosingBonus() {
    return losingBonus;
  }

  public void setLosingBonus(int losingBonus) {
    this.losingBonus = losingBonus;
  }

  public int getPointTotal() {
    return pointTotal;
  }

  public void setPointTotal(int pointTotal) {
    this.pointTotal = pointTotal;
  }
  
  public int compareTo(KClub c) {
    return ((Integer) pointsScored).compareTo(c.pointsScored);
  }
}
