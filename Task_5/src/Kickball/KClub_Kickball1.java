package Kickball;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/******************************************************************************
 * This example demonstrates processing a list of integers with a stream.     
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/


public class KClub_Kickball1 {
  public static void main(String[] args) {
    List<KClub> table = Arrays.asList(
        new KClub(1, "Lumberjacks", 22, 16, 1, 5, 621, 400, 221, 75, 41,
            8, 2, 76),
        new KClub(2, "Hull and Oates", 22, 16, 0, 6, 625, 414, 211, 72, 43, 9, 2, 75),
        new KClub(3, "Bearcats", 22, 15, 1, 6, 453, 421, 32, 37, 39, 4,
            2, 68),
        new KClub(4, "Sharknadoes", 22, 14, 1, 7, 664, 418, 246, 70, 40, 5, 5, 68),
        new KClub(5, "Timberwolves", 22, 14, 0, 8, 663, 437, 226, 70, 46, 5, 7,
            68),
        new KClub(6, "Pine Riders", 22, 11, 2, 9, 672, 527, 145, 77, 54, 9, 4, 61),
        new KClub(7, "Freezer Geezers", 22, 11, 0, 11, 497, 482, 15, 62, 54, 6, 4,
            54),
        new KClub(8, "Money Shots", 22, 10, 0, 12, 444, 514, -70, 45, 50, 4, 5,
            49),
        new KClub(9, "Warlocks", 22, 9, 1, 12, 553, 575, -22, 53, 61, 4, 6,
            48),
        new KClub(10, "Spartans", 22, 7, 1, 14, 442, 578, -136, 46, 57, 4, 6,
            40),
        new KClub(11, "Renegades", 22, 5, 1, 16, 475, 545, -70, 57, 61,
            4, 8, 34),
        new KClub(12, "Mid Ice Crisis", 22, 0, 0, 22, 223, 1021, -798, 29, 147, 1,
            0, 1));

    System.out.println("   clubName                pointTotal    wonTotal  lostTotal   drawnTotal  \n");
    table.forEach(x -> System.out.println(x));
   
    try {
			FileWriter writer = new FileWriter("KClub Output1.txt");
			writer.write("   clubName                pointTotal    wonTotal  lostTotal   drawnTotal  \n");
			writer.write("   ---------                ------    ---------  ----------   ------\n");
			table.forEach(x -> {
			try {
				writer.write(x + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
			writer.close();
			System.out.println("\nKClub Output1.txt file successfully written");
	    } catch (IOException e) {
	    	System.out.println("An error has occurred.");
	    	e.printStackTrace();
	    }
  }

}
