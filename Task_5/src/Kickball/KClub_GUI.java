package Kickball;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class KClub_GUI extends JFrame implements Serializable {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	@SuppressWarnings("deprecation")
	public KClub_GUI() {
		setResizable(false);
		setTitle("6CS002_Task5_2064801");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600, 600);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel title = new JLabel("<html><h1><strong><i>MEN KICKBALL</i></strong></h1><hr></html>");
		title.setBounds(210, 34, 45, 16);
		title.resize(300, 50);
		contentPane.add(title);
		
		JButton kickball1 = new JButton("Men Kickball 1");
		kickball1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KClub_Kickball1.main(null);
			}
		});
		kickball1.setBounds(200, 80, 117, 29);
		kickball1.resize(200, 50);
		kickball1.setBackground(Color.GREEN);
		contentPane.add(kickball1);
		
		JButton kickball2 = new JButton("Men Kickball 2");
		kickball2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KClub_Kickball2.main(null);
			}
		});
		kickball2.setBounds(200, 150, 117, 29);
		kickball2.resize(200, 50);
		kickball2.setBackground(Color.GREEN);
		contentPane.add(kickball2);
		
		JButton kickball3 = new JButton("Men Kickball 3");
		kickball3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KClub_Kickball3.main(null);
			}
		});
		kickball3.setBounds(200, 220, 117, 29);
		kickball3.resize(200, 50);
		kickball3.setBackground(Color.GREEN);
		contentPane.add(kickball3);
		
		JButton kickball4 = new JButton("Men Kickball 4");
		kickball4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KClub_Kickball4.main(null);
			}
		});
		kickball4.setBounds(200, 290, 117, 29);
		kickball4.resize(200, 50);
		kickball4.setBackground(Color.GREEN);
		contentPane.add(kickball4);
		
		JButton kickball5 = new JButton("Men Kickball 5");
		kickball5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KClub_Kickball5.main(null);
			}
		});
		kickball5.setBounds(200, 360, 117, 29);
		kickball5.resize(200, 50);
		kickball5.setBackground(Color.GREEN);
		contentPane.add(kickball5);
		
		JButton exit = new JButton("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		exit.setBounds(200, 440, 117, 29);
		exit.resize(200, 30);
		exit.setBackground(Color.RED);
		contentPane.add(exit);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KClub_GUI frame = new KClub_GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}